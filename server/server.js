const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
const gzippo = require('gzippo');

app.set('port', 9356);

app.use(gzippo.staticGzip(__dirname + '/public'));
app.use(gzippo.compress());

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get([ '*' ], function (request, response) {
  response.sendFile(path.join(__dirname + '/public/index.html'));
});

app.listen(app.get('port'), function () {
  console.log('Node app is running on port', app.get('port'));
});



