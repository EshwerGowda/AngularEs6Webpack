/*@ngInject*/
export default ($stateProvider) => {
    $stateProvider
        .state('home', {
            url: '/',
            template: require('../../views/home.html'),
            controller: 'HomeController',
            controllerAs: 'home'
        });
}
