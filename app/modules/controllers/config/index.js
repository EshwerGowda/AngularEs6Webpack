/**
 * Created by akshay on 27/7/17.
 */
import angular from 'angular';
import uirouter from 'angular-ui-router';

import directives from '../../directives/config';
import services from '../../services/config';

import routes from './routes';
import HomeController from '../home';

export default angular.module('controllers', [uirouter, directives, services])
    .config(routes)
    .controller('HomeController', HomeController)
    .name;
