/**
 * Created by akshay on 25/7/17.
 */

/*@ngInject*/
class OSDetectorService {
    constructor() {
    }

    getDeviceOS() {
        if (/Android/i.test(navigator.userAgent)) {
            return 'Android';
        } else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            return 'iOS';
        } else {
            return '';
        }
    }
}

export default OSDetectorService;