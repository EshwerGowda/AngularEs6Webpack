/**
 * Created by akshay on 26/7/17.
 */

/*@ngInject*/
class NewsletterService {
    constructor($http) {
        this.$http = $http;
    }

    subscribe(subscriptionDetails) {
        return this.$http.post('/api/subscribe', subscriptionDetails);
    }
}

export default NewsletterService;