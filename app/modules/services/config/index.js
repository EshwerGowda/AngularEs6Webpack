/**
 * Created by akshay on 27/7/17.
 */
import angular from 'angular';

import NewsletterService from '../newsletterService';
import OSDetectorService from '../osDetectorService';

export default angular.module('services', [])
    .service('NewsletterService', NewsletterService)
    .service('OSDetectorService', OSDetectorService)
    .name;

