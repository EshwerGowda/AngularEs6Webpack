/**
 * Created by akshay on 24/7/17.
 */

import '../sass/directive/seenOn.scss';

/*@ngInject*/
export default class SeenOn {
    constructor() {
        this.template = require('../views/directive/seenOn.html');
        this.restrict = 'E';
        this.replace = true;
        this.scope = {};
    }

    // optional compile function
    compile(tElement) {
        return this.link.bind(this);
    }

    // optional link function
    link(scope, element, attributes) {
    }
}
