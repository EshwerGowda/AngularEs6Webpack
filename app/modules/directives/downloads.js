/**
 * Created by akshay on 24/7/17.
 */

import '../sass/directive/downloads.scss';

/*@ngInject*/
export default class Downloads {
    constructor() {
        this.template = require('../views/directive/downloads.html');
        this.restrict = 'E';
        this.replace = true;
        this.controller = 'DownloadController';
        this.scope = {
        };

    }

    // optional compile function
    compile(tElement) {
        return this.link.bind(this);
    }

    // optional link function
    link(scope, element, attributes, controller) {
        if (controller.device === 'Android') {
            scope.isAndroidDevice = true;
        } else if (controller.device === 'iOS') {
            scope.isIOSDevice = true;
        }
    }
}
