import CommonController from './controllers/common';
import '../sass/directive/header.scss';

/*@ngInject*/
export default class Header {
    constructor() {
        this.template = require('../views/directive/header.html');
        this.restrict = 'E';
        this.scope = {};
        this.controller = CommonController;
    }

    // optional compile function
    compile(tElement) {
        return this.link.bind(this);
    }

    // optional link function
    link(scope, element, attributes, controller) {
        scope.showBars = true;

        scope.scrollTo = function (id) {
            angular.element('html, body').animate({scrollTop: angular.element('#' + id).offset().top}, 'slow');
        };

        function toggleClass() {
            let headerElement = angular.element('#fixed-header');
            let elementTop = headerElement.offset().top;

            /*TODO : Need to use $window from angular once directive injection issue was resolved*/
            let windowElemtnt = angular.element(window);

            windowElemtnt.scroll(function () {
                headerElement.toggleClass('affix', windowElemtnt.scrollTop() > elementTop);
            });
        }

        toggleClass();
    }
}
