/**
 * Created by akshay on 26/7/17.
 */

import '../sass/directive/main.scss';

/*@ngInject*/
export default class Main {
    constructor() {
        this.template = require('../views/directive/main.html');
        this.restrict = 'E';
        this.replace = true;
        this.scope = {};
    }

    // optional compile function
    compile(tElement) {
        return this.link.bind(this);
    }

    // optional link function
    link(scope, element, attributes) {
    }
}