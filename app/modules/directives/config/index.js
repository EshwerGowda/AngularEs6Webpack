/**
 * Created by akshay on 27/7/17.
 */
import angular from 'angular';

import directiveControllers from '../controllers/config';

import Contact from '../contact';
import Downloads from '../downloads';
import Footer from '../footer';
import Header from '../header';
import Hello from '../hello';
import Main from '../main';
import Newsletter from '../newsletter';
import SeenOn from '../seenOn';

export default angular.module('directives', [directiveControllers])
    .directive('ngContact', () => new Contact())
    .directive('ngDownloads', () => new Downloads())
    .directive('ngFooter', () => new Footer())
    .directive('ngHeader', () => new Header())
    .directive('ngHello', () => new Hello())
    .directive('ngMain', () => new Main())
    .directive('ngNewsletter', () => new Newsletter())
    .directive('ngSeenOn', () => new SeenOn())
    .name;
