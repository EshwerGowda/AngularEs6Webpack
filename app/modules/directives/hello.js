/**
 * Created by akshay on 24/7/17.
 */

import '../sass/directive/hello.scss';

/*@ngInject*/
export default class Hello {
    constructor() {
        this.template = require('../views/directive/hello.html');
        this.restrict = 'E';
        this.replace = true;
        this.scope = {};
    }

    // optional compile function
    compile(tElement) {
        return this.link.bind(this);
    }

    // optional link function
    link(scope, element, attributes) {
    }
}
