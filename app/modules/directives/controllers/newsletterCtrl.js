/**
 * Created by akshay on 27/7/17.
 */
export default class DownloadController {
    constructor($state, NewsletterService) {
        this.subscribe = function (subscriptionDetails) {
            NewsletterService.subscribe(subscriptionDetails).then((response) => {
                $state.go('success');
            },(error) => {
                $state.go('unsuccessful');
            });
        }
    }
}

DownloadController.$inject = ['$state', 'NewsletterService'];