/**
 * Created by akshay on 27/7/17.
 */
export default class DownloadController {
    constructor(OSDetectorService) {
        this.device = OSDetectorService.getDeviceOS();
    }
}

DownloadController.$inject = ['OSDetectorService'];