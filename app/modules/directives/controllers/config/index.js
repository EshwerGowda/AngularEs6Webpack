/**
 * Created by akshay on 27/7/17.
 */
import angular from 'angular';

import CommonController from '../common';
import DownloadController from '../downloadCtrl';
import NewsletterController from '../newsletterCtrl';

export default angular.module('directiveControllers', [])
    .controller('CommonController', CommonController)
    .controller('DownloadController', DownloadController)
    .controller('NewsletterController', NewsletterController)
    .name;

