/**
 * Created by akshay on 24/7/17.
 */

import '../sass/directive/newsletter.scss';

/*@ngInject*/
export default class Newsletter {
    constructor() {
        this.template = require('../views/directive/newsletter.html');
        this.restrict = 'E';
        this.replace = true;
        this.controller = 'NewsletterController';
        this.scope = {
        };
    }

    // optional compile function
    compile(tElement) {
        return this.link.bind(this);
    }

    // optional link function
    link(scope, element, attributes, controller) {
        scope.user = {};
        scope.submitted = false;

        scope.submitForm = function (isValid) {
            scope.submitted = true;
            if(isValid) {
                controller.subscribe(scope.user);
            }
        };
    }
}